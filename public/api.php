<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Middleware\BodyParsingMiddleware;

require __DIR__ . '/../app/ext/vendor/autoload.php';
require_once(__DIR__.'/../app/controller/BookController.php');

$app = AppFactory::create();
$app->addBodyParsingMiddleware();

$app->post('/llibres', function (Request $request, Response $response, $args) {
    $cnt = new BookController();
    $dades = $request->getParsedBody();
    $insertedId = $cnt->insertBook($dades['title'], $dades['author'], $dades['ISBN']);
    $retDades = ["bi" => $insertedId];

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($retDades));
    return $response;
});

//listar
$app->get('/llibres', function (Request $request, Response $response, $args) {
    $cnt = new BookController();
    $list = $cnt->listBooks();
    $retjson = array();
    foreach($list as $b){
        array_push($retjson, $b->toArray());
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($retjson));
    return $response;
});

//detall
$app->get('/llibres/{ID}', function (Request $request, Response $response, $args) {
    $variable1 = $args['ID'];
    $cnt = new BookController();
    $book = $cnt->getBook($variable1);

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($book->toArray()));
    return $response;
});

//update
$app->put('/llibres/{ID}', function (Request $request, Response $response, $args) {
    $variable1 = $args['ID'];
    $cnt = new BookController();
    $dades = $request->getParsedBody();
    $book = $cnt->updateBook($dades['title'], $dades['author'], $dades['ISBN'], $variable1);

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($book->toArray()));
    return $response;
});

//delete
$app->delete('/llibres/{ID}', function (Request $request, Response $response, $args) {
    $variable1 = $args['ID'];
    $cnt = new BookController();
    $book = $cnt->removeBook($variable1);

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($book->toArray()));
    return $response;
});

$app->run();
