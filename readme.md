# API Llibres (projecte educatiu)

## Format dels llibres

```JSON
{
  "ISBN" : "Codi del llibre",
  "title" : "Titol del llibre",
  "author" : "Nom del autor"
}
```

## Comandes cURL utilitzades
A continuació es mostren les comandes cURL utilitzades

### Sense dades en el cos

#### GET
```
curl -i -X GET http://localhost/llibres
curl -i http://localhost/llibres
```

#### POST
```
curl -i -X POST http://localhost/llibres
```

#### PUT
```
curl -i -X PUT http://localhost/llibres
```

#### DELETE
```
curl -i -X DELETE http://localhost/llibres/123456
```

### Amb dades al cos de la petició
En aquesta petició es defineix el verb (-X POST), una capçalera que defineix el tipus de dades que s'envia (-H 'Content-Type: application/json') i les dades del llibre simplificades (--data '{"title" : "Titol del llibre"}'). 

```
curl -i -X POST -H 'Content-Type: application/json' http://localhost/llibres --data '{"title" : "Titol del llibre"}'
```