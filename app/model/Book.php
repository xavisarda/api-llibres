<?php

class Book{

  private $_id;
  private $_title;
  private $_ISBN;
  private $_author;

  public function __construct($t, $i, $a, $bi = null){
    $this->setTitle($t);
    $this->setISBN($i);
    $this->setAuthor($a);
    $this->setId($bi);
  }

  public function getId(){
    return $this->_id;
  }

  public function setId($v){
    $this->_id = $v;
  }

  public function getTitle(){
    return $this->_title;
  }

  public function getISBN(){
    return $this->_ISBN;
  }

  public function getAuthor(){
    return $this->_author;
  }

  public function setTitle($v){
    $this->_title = $v;
  }

  public function setISBN($v){
    $this->_ISBN = $v;
  }

  public function setAuthor($v){
    $this->_author = $v;
  }

  public function toArray(){
    $book = [ "title" => $this->getTitle(),
              "author" => $this->getAuthor(),
              "ISBN" => $this->getISBN() ];
    if($this->getId() != null){
      $book['bi'] = $this->getId();
    }
    return $book;
  }

}
