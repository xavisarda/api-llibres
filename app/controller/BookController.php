<?php

require_once(__DIR__.'/../model/db/BookDb.php');

class BookController{

  public function listBooks(){
    $db = new BookDb();
    return $db->listBooks();
  }

  public function getBook($id){
    $db = new BookDb();
    return $db->getBook($id);
  }

  public function insertBook($title, $author, $isbn){
    $book = new Book($title, $isbn, $author);

    $db = new BookDb();
    return $db->addBook($book);
  }

  public function updateBook($title, $author, $isbn, $id){
    $book = new Book($title, $isbn, $author);

    $db = new BookDb();
    return $db->updateBook($book, $id);
  }

  public function removeBook($id){
    $db = new BookDb();
    return $db->deleteBook($id);
  }
}