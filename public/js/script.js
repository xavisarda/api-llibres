$(document).ready(function(){
  $('#updatebtn').hide();
  $('#back2new').hide();
  loadFooter();
  loadBookListAjax();
  $('#wrapper').addClass('red');
});

function loadFooter(){
  $('#footer').load('/fragments/footer.html');
}

function loadBookList(){
  $.getJSON('/llibres',function(data){
    var llibreshtml = '<ul>';
    $.each(data, function(key, value){
      llibreshtml += '<li>'+value.title+'</li>';
    });
    llibreshtml += '</ul>';
    $('#main-content').html(llibreshtml);
  });
}

function loadBookListAjax(){
  $.ajax({
    accepts: 'application/json',
    contentType: 'application/json; charset=UTF-8',
    url: '/llibres',
    method: 'GET',
    success: function(data){
      var llibreshtml = '<ul>';
      $.each(data, function(key, value){
        llibreshtml += '<li><a onclick="showBook(\''+value.bi+'\')">'+value.title+'</a>'
        +' - <a onclick="removeBook(\''+value.bi+'\')">delete</a>'
        +' - <a onclick="loadUpdateData(\''+value.bi+'\')">update</a></li>';
      });
      llibreshtml += '</ul>';
      $('#main-content').html(llibreshtml);
    }
  });
}

function createBook(){
  // default data var book = { "title": "Ajax", "ISBN": "122345656", "author":"Ajax Author" };
  var book = { "title": $('#btitle').val(), 
      "ISBN": $('#bisbn').val(), 
      "author": $('#bauth').val() };
  $.ajax({
    accepts: 'json',
    contentType: 'application/json',
    data: JSON.stringify(book),
    dataType: "json",
    url: '/llibres',
    method: 'POST',
    success: function(data){
      loadBookListAjax();
      clearInputs();
    }
  });
}

function showBook(bookid){
  $.ajax({
    accepts: 'json',
    contentType: 'application/json',
    url: '/llibres/'+bookid,
    method: 'GET',
    success: function(data){
      var llibreshtml = '<h3>'+data.title+'</h3>';
      llibreshtml += '<ul>';
      llibreshtml += '<li>'+data.author+'</li>';
      llibreshtml += '<li>'+data.ISBN+'</li>';
      llibreshtml += '</ul>';
      $('#detail-content').html(llibreshtml);
    }
  });
}

function updateBook(){
  var book = { "title": $('#btitle').val(), 
      "ISBN": $('#bisbn').val(), 
      "author": $('#bauth').val() };
  $.ajax({
    accepts: 'json',
    contentType: 'application/json',
    data: JSON.stringify(book),
    dataType: "json",
    url: '/llibres/'+$('#bid').val(),
    method: 'PUT',
    success: function(data){
      loadBookListAjax();
      backToAdd();
    }
  });
}

function removeBook(bookid){
  $.ajax({
    accepts: 'json',
    contentType: 'application/json',
    url: '/llibres/'+bookid,
    method: 'DELETE',
    success: function(data){
      loadBookListAjax();
    }
  });
}

function clearInputs(){
  $('#btitle').val("");
  $('#bisbn').val("");
  $('#bauth').val("");
  $('#bid').val("");
}

function loadUpdateData(bookid){
  $.ajax({
    accepts: 'json',
    contentType: 'application/json',
    url: '/llibres/'+bookid,
    method: 'GET',
    success: function(data){
      $('#btitle').val(data.title);
      $('#bisbn').val(data.ISBN);
      $('#bauth').val(data.author);
      $('#bid').val(bookid);
      $('#createbtn').hide();
      $('#updatebtn').show();
      $('#back2new').show();
    }
  });
}

function backToAdd(){
  clearInputs();
  $('#createbtn').show();
  $('#updatebtn').hide();
  $('#back2new').hide();
}
