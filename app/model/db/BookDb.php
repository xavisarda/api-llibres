<?php

require_once(__DIR__.'/../Book.php');

class BookDb{

  public function addBook($book){
    $collection = $this->openConnection();
    $insertBook = $collection->insertOne($book->toArray());

    return $insertBook->getInsertedId();
  }

  public function getBook($id){
    $collection = $this->openConnection();
    $book = $collection->findOne(['_id' => new MongoDB\BSON\ObjectId($id)]);

    return new Book($book['title'], $book['ISBN'], 
        $book['author'],$book['_id']->__toString());
  }

  public function listBooks(){
    $collection = $this->openConnection();
    $books = $collection->find();

    $retarr = array();
    foreach($books as $book){
      array_push($retarr, new Book($book['title'], $book['ISBN'], 
        $book['author'],$book['_id']->__toString()));
    }

    return $retarr;
  }

  public function updateBook($book, $id){
    $collection = $this->openConnection();
    $updatestmt = [ '$set' => $book->toArray() ];
    unset($updatestmt['bi']);
    $updatefilter =  ['_id' => new MongoDB\BSON\ObjectId($id)];
    $updatep = $collection->updateOne($updatefilter, $updatestmt);

    return $this->getBook($id);
  }

  public function deleteBook($id){
    $retdata = $this->getBook($id);
    $collection = $this->openConnection();
    $removep = $collection->deleteOne(['_id' => new MongoDB\BSON\ObjectId($id)]);

    return $retdata;
  }

  public function openConnection(){
    return (new MongoDB\Client)->books->book;
  }
}